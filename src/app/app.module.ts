import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { ArticulosComponent } from './articulos/articulos.component';
import { DetalleArticuloComponent } from './detalle-articulo/detalle-articulo.component';
import { AdminComponent } from './admin/admin.component';
import { AsignarParComponent } from './admin/asignar-par/asignar-par.component';
import { EvaluarArticuloComponent } from './admin/evaluar-articulo/evaluar-articulo.component';
import { RegistroComponent } from './registro/registro.component';

const routes:Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent},
  { path: 'login', component: LoginComponent},
  { path: 'registro', component: RegistroComponent},
  { path: 'articulos', component: ArticulosComponent},
  { path: 'articulos/:id', component: DetalleArticuloComponent},
  { path: 'admin', component: AdminComponent},
  { path: 'admin/articulos/:id',
    children: [
      {path:'asignar-par', component: AsignarParComponent},
      {path:'evaluar', component: EvaluarArticuloComponent},
    ]
  }
];

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    HomeComponent,
    LoginComponent,
    ArticulosComponent,
    DetalleArticuloComponent,
    AdminComponent,
    AsignarParComponent,
    EvaluarArticuloComponent,
    RegistroComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
