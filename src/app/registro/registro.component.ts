import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  checkRegister(name,email,password){
  	if (name.value.length>0 && email.value.length>0 && password.value.length>0) {
  		alert('Registro completo');
  		this.router.navigate(['/admin']);
  	}else{
  		alert('Información incompleta. Revise el formulario.')
  	}
  }

}
