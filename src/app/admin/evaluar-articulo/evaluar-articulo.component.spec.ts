import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EvaluarArticuloComponent } from './evaluar-articulo.component';

describe('EvaluarArticuloComponent', () => {
  let component: EvaluarArticuloComponent;
  let fixture: ComponentFixture<EvaluarArticuloComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EvaluarArticuloComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EvaluarArticuloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
